class TableCourseAddColumns < ActiveRecord::Migration
  def up
  	add_column("courses", "professor", :string, :limit => 50)
  	add_column("courses", "term", :string, :limit => 3)
  	add_column("courses", "year", :integer)
  	add_column("courses", "section", :string, :limit => 1)
  end

  def down
  	remove_column("courses", "professor")
  	remove_column("courses", "term")
  	remove_column("courses", "year")
  	remove_column("courses", "section")
  end
end
