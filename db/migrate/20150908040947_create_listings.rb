class CreateListings < ActiveRecord::Migration
  def up
    create_table :listings do |t|

      	t.boolean "sold", :default => false, :null => false
      	t.date "expiry", :null => false
        t.text "message"

        t.references :user

      	t.timestamps null: false
    end

    add_index :listings, "user_id"
  end

  def down 
  	drop_table :listings
  end
end
