class TableUserAddColumnValidatedPhoneRatingListings < ActiveRecord::Migration
  def up
  	add_column :users, :validated, :boolean, default: false
  	add_column :users, :phone, :string, limit: 13
  	add_column :users, :rating, :integer, limit: 5
  	add_column :users, :listings_posted, :integer
  end

  def down
  	remove_column :users, :listings_posted
  	remove_column :users, :rating
  	remove_column :users, :phone
  	remove_column :users, :validated
  end
end


