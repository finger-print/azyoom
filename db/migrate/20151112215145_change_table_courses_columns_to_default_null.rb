class ChangeTableCoursesColumnsToDefaultNull < ActiveRecord::Migration
  def up
  	change_column_default :courses, :name, nil
  	change_column_default :courses, :professor, nil
  	change_column_default :courses, :term, nil
  	change_column_default :courses, :year, nil
  	change_column :courses, :section, :string, limit: 2
  end

  def down
  	change_column :courses, :section, :string, limit: 1
  end
end
