class CreateCourses < ActiveRecord::Migration
  def up
    create_table :courses do |t|

    	t.string "university", :limit => 50, :null => false
    	t.string "department", :limit => 5 , :null => false
    	t.integer "course_no", :null => false

      t.references :listing

  		t.timestamps null: false
    end

    add_index :courses, "listing_id" 
  end

  def down 
  	drop_table :courses
  end
end
