class TableCoursesAddColumnCourseName < ActiveRecord::Migration
  def up
  	add_column("courses", "name", :string, :limit => 100, :after => "course_no")
  end

  def down 
  	remove_column("courses", "name")
  end
end
