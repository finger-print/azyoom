class CreateBooks < ActiveRecord::Migration
  def up
    create_table :books do |t|
  		t.string "name", :limit => 100, :null => false
  		t.string "authors", :limit => 70, :null => false
  		t.integer "edition"
  		t.string "publication", :limit => 100
  		t.decimal "price", :precision => 6, :scale => 2, :null => false
  		t.boolean "used"

      t.references :listing

    	t.timestamps null: false
    end

    add_index :books, "listing_id"
  end

  def down 
  	drop_table :books
  end
end
