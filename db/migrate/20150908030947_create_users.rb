class CreateUsers < ActiveRecord::Migration
  def up
    create_table :users do |t|

      t.string 'first_name', :null => false, :limit => 25
      t.string 'last_name', :null => false, :limit => 50
      t.string 'email', :null => false, :limit => 254
      t.string "password_digest", :null => false
      t.string "university", :null => false, :limit => 50
      t.string "program", :limit => 100

      t.timestamps null: false
    end

   	#change_column :users, "user_id", :string
  end

  def down
  	drop_table :users
  end
end
