# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151118050335) do

  create_table "books", force: :cascade do |t|
    t.string   "name",        limit: 100,                         null: false
    t.string   "authors",     limit: 70,                          null: false
    t.integer  "edition",     limit: 4
    t.string   "publication", limit: 100
    t.decimal  "price",                   precision: 6, scale: 2, null: false
    t.boolean  "used",        limit: 1
    t.integer  "listing_id",  limit: 4
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "books", ["listing_id"], name: "index_books_on_listing_id", using: :btree

  create_table "courses", force: :cascade do |t|
    t.string   "university", limit: 50,  null: false
    t.string   "department", limit: 5,   null: false
    t.integer  "course_no",  limit: 4,   null: false
    t.string   "name",       limit: 100
    t.integer  "listing_id", limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "professor",  limit: 50
    t.string   "term",       limit: 3
    t.integer  "year",       limit: 4
    t.string   "section",    limit: 2
  end

  add_index "courses", ["listing_id"], name: "index_courses_on_listing_id", using: :btree

  create_table "events", force: :cascade do |t|
    t.datetime "datetime",                 null: false
    t.string   "name",       limit: 63
    t.string   "venue",      limit: 127,   null: false
    t.text     "agenda",     limit: 65535
    t.integer  "frequency",  limit: 2
    t.integer  "group_id",   limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "events", ["group_id"], name: "index_events_on_group_id", using: :btree

  create_table "group_members", id: false, force: :cascade do |t|
    t.integer  "group_id",   limit: 4
    t.integer  "user_id",    limit: 4
    t.integer  "status",     limit: 1, default: 1, null: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "group_members", ["group_id"], name: "index_group_members_on_group_id", using: :btree
  add_index "group_members", ["user_id"], name: "index_group_members_on_user_id", using: :btree

  create_table "groups", force: :cascade do |t|
    t.string   "name",        limit: 50,    null: false
    t.text     "description", limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "course_id",   limit: 4
  end

  add_index "groups", ["course_id"], name: "index_groups_on_course_id", using: :btree

  create_table "listings", force: :cascade do |t|
    t.boolean  "sold",       limit: 1,     default: false, null: false
    t.date     "expiry",                                   null: false
    t.text     "message",    limit: 65535
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  add_index "listings", ["user_id"], name: "index_listings_on_user_id", using: :btree

  create_table "posts", force: :cascade do |t|
    t.string   "title",      limit: 127,   null: false
    t.text     "text",       limit: 65535, null: false
    t.integer  "group_id",   limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "first_name",      limit: 25,                  null: false
    t.string   "last_name",       limit: 50,                  null: false
    t.string   "email",           limit: 254,                 null: false
    t.string   "password_digest", limit: 255,                 null: false
    t.string   "university",      limit: 50,                  null: false
    t.string   "program",         limit: 100
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.boolean  "validated",       limit: 1,   default: false
    t.string   "phone",           limit: 255
    t.integer  "rating",          limit: 8
    t.integer  "listings_posted", limit: 4
  end

end
