class BooksController < ApplicationController
  def new
  	@book = Book.new({ used: true, price: 0.00 })
  end

  def create
  	@book = Book.new(book_params)

  	if @book.save
  		redirect_to controller: 'pages', action: 'latest'
  	else
  		render 'new'
  	end
  end

  def edit
    @book = Book.find(params[:book_id])
  end

  def update
    @book = Book.find(params[:book_id])
    @book.update_attributes(book_params)

    if @book.save
      redirect_to controller: 'listings', action: 'show'
    else
      render 'edit'
    end
  end

  def delete
  end

  private
  	def book_params
		params.require(:book).permit(:name, :authors, :publication, :edition, :price, :used)
  	end
end
