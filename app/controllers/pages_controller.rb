class PagesController < ApplicationController

  # layout false
  before_action :session_timed_out?, except: [:logout]

  def post
    redirect_to controller: 'listings', action: 'new'
  end

  def about
  end

  def latest
    redirect_to(controller: 'listings', action: 'index')
  end

  def groups
    redirect_to controller: 'groups', action: 'home'
  end

  def login
    redirect_to controller: 'access', action: 'login'
  end

  def logout
    flash[:notice] = "Logged out."
    redirect_to controller: 'access', action: 'logout'
  end

  def register
    redirect_to controller: 'users', action: 'new'
  end
end
