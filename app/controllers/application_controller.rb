class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  private

    # Checks if the user id in the parameters being sent is the
    # same as the user id in session.
    # Used to allow/deny access to private information.
    def user_in_session?
      if !session[:user_id].to_i.eql? params[:user_id].to_i
        redirect_to root_path
        return false
      end
      return true
    end

    # Checks if the session has timed out. Logs the user out
    # if it has.
    def session_timed_out?
      if session[:user_id]
        if Time.now > session[:timeout].to_time 
          flash[:error] = "Session timed out."
          redirect_to controller: 'access', action: 'logout'
          return true
        end
        update_session
        return false
      end
      return false
    end

    # Extends session timeout time any time the user sends a
    # request. 
    def update_session
      if session[:user_id]
        half_hour = 60 * 30
        session[:timeout] = Time.now.since(half_hour).to_s
      end
    end

    # Checks if a user is in session. If a user is in session,
    # it is redirected to the root page.
    # Meant to deny access to pages that require specifically
    # no user to be in session.
    def logged_out?
      if session[:user_id]
        redirect_to root_path
        return false
      end
      return true
    end

end
