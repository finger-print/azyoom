class AccessController < ApplicationController

  before_action :logged_out?, only: [:login, :attempt_login]

  # if user id is given, log the user in
  def login
    user = User.find_by_id(params[:user_id])
    if !user.nil?
      session[:user_id] = user.id
      session[:name] = "#{user.first_name} #{user.last_name}"
      update_session
      redirect_to :controller => 'users', :action => 'show', :user_id => user.id
    end
  end

  # check if user with given email exists
  # check if password matches 
  # if both match, send user id to login action
  def attempt_login
  	if !params[:email].blank? & !params[:password].blank?
  		user = User.find_by_email(params[:email])
  		if !user.nil? 
  			authorized = user.authenticate(params[:password])
  			if authorized
  				redirect_to :action => 'login', :user_id => user.id
  			else
  				flash[:error] = "Email or password did not match our records."
          redirect_to :action => 'login'
  			end
  		else
  			flash[:error] = "User with specified email does not exist."
        redirect_to :action => 'login'
  		end
  	else
  		flash[:error] = "Can not leave email or password blank."
      redirect_to :action => 'login'
  	end
  end

  # update session user name after editing user attributes
  def update_name
    session[:name] = "#{params[:first_name]} #{params[:last_name]}"
    update_session
    redirect_to :controller => 'users', :action => 'show', :user_id => params[:user_id]
  end

  # log user out of session
  def logout
  	session[:user_id] = nil
    session[:name] = nil
    session[:timeout] = nil
    redirect_to root_path
  end
end
