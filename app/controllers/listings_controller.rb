class ListingsController < ApplicationController

  before_action :session_timed_out?
  before_action :listing_exists?, only: [:show, :edit, :update, :delete, :destroy]
  before_action :session_user_posted_listing?, only: [:edit, :update, :delete, :destroy]
  before_action :listing_params?, only: :create
  
  def search
    keyword = "%#{params[:keyword]}%"
    @listings = Listing.joins(:course).where(['department LIKE :keyword OR course_no LIKE :keyword OR name LIKE :keyword OR professor LIKE :keyword', {  keyword: keyword }]) | Listing.joins(:book).where(['name LIKE :keyword OR authors LIKE :keyword OR publication LIKE :keyword', {  keyword: keyword }])
  end

  def index
    @listings = Listing.all
  end

  def show
    listing = Listing.find_by_id(params[:listing_id])
    @listing_id = listing.id
    @book = listing.book
    @course = listing.course
    @user = listing.user

    @of_current_user = false
    if session[:user_id]
      if session[:user_id].to_s.eql? @user.id.to_s
        @of_current_user = true
      end
    end
  end

  def new
    @listing = Listing.new
    @listing.book = Book.new
    @listing.course = Course.new
  end

  def create
    @listing = Listing.new(listing_params)
    @listing.expiry = Date.current.next_year

    unless session[:user_id].nil?
      @listing.user_id = session[:user_id].to_i
    end

    if @listing.save
      redirect_to controller: 'listings', action: 'index'
    else
      render 'new'
    end
  end

  def edit
    @listing = Listing.find_by_id(params[:listing_id])
  end

  def update
    @listing = Listing.find(params[:listing_id])
    @listing.update_attributes(listing_params)

    if @listing.save
      redirect_to :controller => 'listings', :action => 'show', listing_id: @listing.id
    else
      render 'edit'
    end
  end

  def delete
    redirect_to :action => 'destroy', :listing_id => params[:listing_id]
  end

  def destroy 
    @listing = Listing.destroy(params[:listing_id])
    @course = Course.destroy(@listing.course)
    @book = Book.destroy(@listing.book)
    
    redirect_to root_path
  end

  private 

    # white-listing parameters
    def listing_params
      params.require(:listing)
            .permit(book_attributes: [:name, :authors, :edition, :publication, :price, :used],
                    course_attributes: [:university, :department, :course_no, :name, :professor, :term, :year, :section])
    end

    # check if any parameters have been passed in
    def listing_params?
      if !params[:listing]
        redirect_to root_path
        return false
      end
      return true
    end

    # check if the user in session is the owner of the listing
    def session_user_posted_listing?
      if params[:listing_id]
        user = Listing.find(params[:listing_id]).user
        if !user.nil?
          if session[:user_id].to_i.eql? user.id.to_i
            return true
          end
        end
      end
      redirect_to root_path
      return false
    end

    # check if listing with given id exists
    def listing_exists?
      if !Listing.find_by_id(params[:listing_id])
        redirect_to root_path
        return false
      end
      return true
    end

    # return ids of all listings that belong to the user in session
    def user_listing_ids
      if logged_in?
        user_listings = Listing.user_listings(session[:user_id])
        @user_listing_ids = user_listings.map { |listing| listing.id }
      end
    end
end
