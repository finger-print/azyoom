class CoursesController < ApplicationController
  def new
  	@course = Course.new({ university: "Carleton University" })
  end

  def create
  	@course = Course.new(course_params)

  	if @course.save 
  		redirect_to controller: 'pages', action: 'latest'
  	else
  		render 'new'
  	end
  end

  def edit
    @course = Course.find(params[:course_id])
  end

  def udpate
    course = Course.find(params[:course_id])
    course.update_attributes(course_params)

    if course.save
      redirect_to controller: 'listings', action: 'show', listing_id: course.listing_id
    else
      render 'edit'
    end
  end

  def delete
  end

  private
  	def course_params
  		params.require(:course).permit(:university, :department, :course_no)
  	end
end
