class UsersController < ApplicationController

  before_action :session_timed_out?
  before_action :user_exists?, :user_in_session?, only: [:show, :edit, :update, :delete, :destroy]
  before_action :logged_out?, only: [:new, :create]
  before_action :user_params?, only: :create

  def new
  	@user = User.new
  end

  def create 
    @user = User.new(user_params)

    capitalize @user

    if @user.save
      redirect_to controller: 'access', :action => 'login', :user_id => @user.id
    else
      render 'new'
    end
  end

  def index 
    @users = User.all 
  end

  def show
  	@user = User.find_by_id(params[:user_id])

    @user_listings = Listing.user_listed(@user.id)
  end

  def edit
  	@user = User.find(params[:user_id])
  end

  def udpate

    @user = User.find(params[:user_id])
    @user.update_attributes(user_params)

    if @user.save
      redirect_to :controller => 'access', :action => 'update_name', :first_name => @user.first_name, :last_name => @user.last_name, :user_id => @user.id
    else
      render 'edit'
    end

  end

  def delete
    redirect_to :action => 'destroy', :user_id => params[:user_id]
  end

  def destroy 
    @user = User.destroy(params[:user_id])
    redirect_to controller: 'access', action: 'logout'
  end

  private 

    # white-listing parameters
    def user_params
      params.require(:user)
            .permit(:first_name, :last_name, :email, :password, :university, :program)
    end

    # check if any parameters have been passed in
    def user_params?
      if !params[:user]
        redirect_to root_path
      end
    end

    # capitalize user name - first and last
    def capitalize (user)
      user.first_name.capitalize!
      user.last_name.capitalize!
    end

    # check if user with given id exists
    def user_exists?
      if !User.find_by_id(params[:user_id])
        redirect_to root_path
        return false
      end
      return true
    end

end
