class GroupsController < ApplicationController

  before_action :divert, except: [:home, :show, :new, :create, :edit]

  def home
    flash.now[:error] = "Functionality for Groups has not completed implementation yet. Please be patient while we work hard to bring Groups to your convenient use."
  end

  def new
    @group = Group.new
  end

  def create
    @group = Group.new(group_params)

    if @group.save

      member = GroupMember.new
      member.user_id = session[:user_id]
      member.group_id = @group.id

      member.save     
      redirect_to controller: 'groups', action: 'show', group_id: @group.id
    else
      render 'new'
    end
  end

  def show
    @group = Group.find_by_id(params[:group_id])
    member_ids = GroupMember.where(['group_id LIKE ?', @group.id]).map { |m| m.user_id }
    @members = User.where(id: member_ids)
  end

  def index
  end

  def edit
    @group = Group.find_by_id(params[:group_id])
  end

  def update
  end

  def delete
  end

  def destroy
  end

  private

    def group_params
      params.require(:group).permit(:name, :description)
    end

    def divert
      redirect_to root_path
    end
end
