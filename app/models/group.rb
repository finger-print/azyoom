class Group < ActiveRecord::Base

	validates :name, presence: true

	has_one :course
	has_many :users, through: :group_member
end
