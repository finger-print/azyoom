class Book < ActiveRecord::Base

	validates :name, presence: true, length: { maximum: 100 }
	validates :authors, presence: true, length: { maximum: 150 }
	validates :edition, presence: false
	validates :publication, presence: false
	validates :price, presence: true
	validates :used, inclusion: { within: [true,false] }

	belongs_to :listing

	scope :cheapest, lambda { order("books.price ASC") }
	scope :last_published, lambda { order("books.created_at DESC") }
	scope :used?, lambda { where(:used => true) }
	scope :latest_edition, lambda { order("books.edition") }

	# search scopes
	scope :search_by_name, lambda { |query|
		where(["name LIKE ?", "%#{query}%"])
	}
	scope :search_by_author, lambda { |query|
		where(["authors like ?", "%#{query}%"])
	}
	scope :search_by_publication, lambda { |query|
		where(["publication like ?", "%#{query}%"])
	}
	scope :search_by_lid, lambda { |query|
		where(["listing_id LIKE ?", "%#{query}%"])
	}

end
