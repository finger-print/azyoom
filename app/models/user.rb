class User < ActiveRecord::Base

	has_secure_password

	validates :first_name, presence: true, length: { maximum: 25 }
	validates :last_name, presence: true, length: { maximum: 50 }
	validates :first_name, :last_name, format: { with: /\A[a-zA-Z]+\z/ } 
	validates :email, presence: true, length: { maximum: 100 }, uniqueness: true, format: { with: /@/ }
	validates :password, length: { in: 6..16 }
	validates :university, presence: true, length: { maximum: 50 }
	validates :program, length: { maximum: 100 }


	has_many :listings
	has_many :groups, through: :group_member

	scope :group_members, lambda { |user_ids|
		where(["id IN ?", user_ids])
	}
end
