class Listing < ActiveRecord::Base

	validates :sold, inclusion: { in: [true, false] }

	has_one :book
	accepts_nested_attributes_for :book, allow_destroy: true
	validates_associated :book

	has_one :course
	accepts_nested_attributes_for :course, allow_destroy: true
	validates_associated :course

	belongs_to :user

	# accepts_nested_attributes_for :book, :course

	scope :user_listed, lambda { |user_id|
		where(["user_id LIKE ?", "#{user_id}"])
	}

end
