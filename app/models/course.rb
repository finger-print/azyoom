class Course < ActiveRecord::Base

	validates :university, presence: true
	validates :department, presence: true, length: { in: 3..5 }
	validates :course_no, presence: true, length: { in: 3..4 }
	validates :name, presence: false, length: { maximum: 100 }
	validates :professor, presence: false, length: { maximum: 50 }, allow_nil: true
	validates :term, presence: false, length: { maximum: 3 }, inclusion: { within: %w(FAL WIN SUM) }, allow_nil: true
	validates :year, presence: false, length: { maximum: 4 }
	validates :section, presence: false, length: { maximum: 2 }

	belongs_to :listing

	scope :search_by_university, lambda { |query|
		where(["university LIKE ?", "%#{query}%"])
	} 
	scope :search_by_department, lambda { |query|
		where(["department LIKE ?", "%#{query}%"])
	}
	scope :search_by_course, lambda { |query|
		where(["course_no LIKE ?", "%#{query}%"])
	}
	scope :search_by_lid, lambda { |query|
		where(["listing_id LIKE ?", "%#{query}%"])
	}

end
